﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShiQuan.WinConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Sockets.SocketServer server = new Sockets.SocketServer("127.0.0.1", 30000);
            if (server.Start())
            {
                Console.ReadLine();
            }
            server.Close();
            Environment.Exit(0);
        }
    }
}
