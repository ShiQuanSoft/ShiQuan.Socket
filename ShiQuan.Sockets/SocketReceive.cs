﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace ShiQuan.Sockets
{
    /// <summary>
    /// 
    /// </summary>
    public class SocketReceive
    {
        public string Id { get; set; }

        public byte[] Buffer = new byte[1024 * 1024];

        public Socket Socket { get; set; }
    }
}
