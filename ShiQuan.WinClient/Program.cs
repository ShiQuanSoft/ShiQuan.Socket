﻿using ShiQuan.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShiQuan.WinClient
{
    class Program
    {
        static void Main(string[] args)
        {
            SocketClient client = new SocketClient("127.0.0.1", 30000);
            try
            {
                if (client.Connect())
                {
                    string send = string.Empty;
                    do
                    {
                        Console.WriteLine("发送信息：");
                        send = Console.ReadLine();
                        string result = string.Empty;
                        if (client.Send(send, out result))
                            Console.WriteLine(result);
                    } while (send != "exit");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("请求服务器异常：" + ex.Message);
            }
            Console.ReadLine();
            Environment.Exit(0);
        }
    }
}
